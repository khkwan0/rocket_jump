﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsController : MonoBehaviour {

    public float forceMultiplier;
    private CanvasController cc;
    public float rocketSpeed;
	// Use this for initialization
	void Start () {
        cc = GameObject.Find("Canvas").GetComponent<CanvasController>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.KeypadPlus))
        {
            forceMultiplier += 1f;
            cc.SetForceMultiplerText(forceMultiplier);
        }
        if (Input.GetKeyDown(KeyCode.KeypadMinus))
        {
            forceMultiplier -= 1f;
            cc.SetForceMultiplerText(forceMultiplier);
        }
        if (Input.GetKeyDown(KeyCode.PageUp))
        {
            rocketSpeed += 1f;
            cc.SetRocketSpeedText(rocketSpeed);
        }
        if (Input.GetKeyDown(KeyCode.PageDown))
        {
            rocketSpeed -= 1f;
            cc.SetRocketSpeedText(rocketSpeed);
        }
	}
}
