﻿using System;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    [Serializable]
    public class AdvancedSettings
    {
        public float groundCheckDistance = 0.01f; // distance for checking if the controller is grounded ( 0.01f seems to work best for this )
        public float stickToGroundHelperDistance = 0.5f; // stops the character
        public float slowDownRate = 20f; // rate at which the controller comes to a stop when there is no input
        public bool airControl; // can the user control the direction that is being moved in the air
        [Tooltip("set it to 0.1 or more if you get stuck in wall")]
        public float shellOffset; //reduce the radius by that ratio to avoid getting stuck in wall (a value of 0.1f is nice)
    }

    [SerializeField]
    private float speed = 5f;
    [SerializeField]
    private float lookSensitivity = 3f;
    [SerializeField]
    private float jumpSpeed = 1f;

    private float invertMouse = -1f;

    private PlayerMotor motor;
    private bool m_Jump, m_PreviouslyGrounded, m_Jumping, m_IsGrounded;
    private Vector3 m_GroundContactNormal;
    private CapsuleCollider m_Capsule;

    private Vector3 originalPos;
    private Quaternion originalRot;

    public AdvancedSettings advancedSettings = new AdvancedSettings();
    private void Start()
    {
        motor = GetComponent<PlayerMotor>();
        m_Capsule = GetComponent<CapsuleCollider>();
        originalPos = transform.position;
        originalRot = transform.rotation;
    }

    private void Update()
    {
        float _xMov = Input.GetAxisRaw("Horizontal");
        float _zMov = Input.GetAxisRaw("Vertical");

        Vector3 _movHorizontal = transform.right * _xMov;
        Vector3 _movVertical = transform.forward * _zMov;

        Vector3 _velocity = (_movHorizontal + _movVertical).normalized * speed;

        motor.Move(_velocity);

        float _yRot = Input.GetAxisRaw("Mouse X");
        float _xRot = Input.GetAxisRaw("Mouse Y");

        Vector3 _rotation = new Vector3(0f, _yRot, 0f) * lookSensitivity;
        float _cameraRotationX = _xRot * lookSensitivity * invertMouse;
        motor.Rotate(_rotation);
        motor.RotateCamera(_cameraRotationX);

        if (Input.GetKeyDown(KeyCode.Space) && m_IsGrounded)
        {
            motor.Jump(jumpSpeed);
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            invertMouse *= -1f;
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            resetPosition();
        }
    }

    private void FixedUpdate()
    {
        GroundCheck();
    }

    void resetPosition()
    {
        gameObject.transform.position = originalPos;
        gameObject.transform.rotation = originalRot;
    }

    /// sphere cast down just beyond the bottom of the capsule to see if the capsule is colliding round the bottom
    private void GroundCheck()
    {
        m_PreviouslyGrounded = m_IsGrounded;
        RaycastHit hitInfo;
        if (Physics.SphereCast(transform.position, m_Capsule.radius * (1.0f - advancedSettings.shellOffset), Vector3.down, out hitInfo,
                               ((m_Capsule.height / 2f) - m_Capsule.radius) + advancedSettings.groundCheckDistance, Physics.AllLayers, QueryTriggerInteraction.Ignore))
        {
            m_IsGrounded = true;
            m_GroundContactNormal = hitInfo.normal;
        }
        else
        {
            m_IsGrounded = false;
            m_GroundContactNormal = Vector3.up;
        }
        if (!m_PreviouslyGrounded && m_IsGrounded && m_Jumping)
        {
            m_Jumping = false;
        }
    }
}
