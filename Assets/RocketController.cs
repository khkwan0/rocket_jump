﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketController : MonoBehaviour {

    public GameObject explosion;

    PhysicsController pc;

    private Rigidbody rb;
	// Use this for initialization
	void Start () {
        pc = GameObject.Find("PhysicsController").GetComponent<PhysicsController>();
        rb = GetComponent<Rigidbody>();
        Destroy(gameObject, 10f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void FixedUpdate()
    {
        rb.MovePosition(rb.GetComponent<Rigidbody>().position + rb.transform.up * pc.rocketSpeed * Time.fixedDeltaTime);

    }

    private void OnCollisionEnter(Collision collision)
    {
        GameObject _explosion = GameObject.Instantiate(explosion, collision.contacts[0].point, Quaternion.identity);
        Destroy(_explosion, 5f);
        Destroy(gameObject);
    }
}
