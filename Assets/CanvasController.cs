﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasController : MonoBehaviour {

    public Text forceMultiplierText;
    public Text rocketSpeedText;
    private PhysicsController pc;
	// Use this for initialization
	void Start () {
        pc = GameObject.Find("PhysicsController").GetComponent<PhysicsController>();
        forceMultiplierText.text = "Force Multiplier = " + pc.forceMultiplier.ToString();
        rocketSpeedText.text = "Rocket Speed = " + pc.rocketSpeed.ToString();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetForceMultiplerText(float amt)
    {
        Debug.Log(amt);
        forceMultiplierText.text = "Force Multiplier = " + amt.ToString();
    }

    public void SetRocketSpeedText(float amt)
    {
        rocketSpeedText.text = "Rocket Speed = " + amt.ToString();
    }
}
