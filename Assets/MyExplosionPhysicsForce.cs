using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets.Effects
{
    public class MyExplosionPhysicsForce : MonoBehaviour
    {
        public float explosionForce = 4;
        private PhysicsController pc;
        private float _forceMultiplier;
        private Vector3 _direction;
        private float _magnitude;

        private IEnumerator Start()
        {
            _forceMultiplier = 1f;
            pc = GameObject.Find("PhysicsController").GetComponent<PhysicsController>();            
            // wait one frame because some explosions instantiate debris which should then
            // be pushed by physics force
            yield return null;


            float multiplier = GetComponent<ParticleSystemMultiplier>().multiplier;

            float r = 10*multiplier;
            var cols = Physics.OverlapSphere(transform.position, r);
            var rigidbodies = new List<Rigidbody>();
            foreach (var col in cols)
            {
                if (col.attachedRigidbody != null && !rigidbodies.Contains(col.attachedRigidbody) && col.gameObject.tag != "Ammo")
                {
                    rigidbodies.Add(col.attachedRigidbody);
                }
            }
            foreach (var rb in rigidbodies)
            {
                Debug.Log(rb.gameObject.tag);
                if (rb.gameObject.tag != "Ammo")
                {
                    if (rb.gameObject.tag == "Player")
                    {
                        _direction = rb.gameObject.transform.position - gameObject.transform.position;
                        _magnitude = _direction.magnitude;
                        if (_magnitude <0.1f)
                        {
                            _magnitude = 0.1f;
                        }
                        Debug.Log(_direction.normalized);
                        Debug.Log(_magnitude);
                        rb.velocity = pc.forceMultiplier * _direction.normalized / _magnitude;
                    }
                    else
                    {
                        rb.AddExplosionForce(explosionForce * multiplier, transform.position, r, 1 * multiplier, ForceMode.Impulse);
                    }                    
                }
            }
        }
    }
}
