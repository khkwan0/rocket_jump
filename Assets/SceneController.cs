﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour {

    public GameObject groundTile;

	// Use this for initialization
	void Start () {
        CreateGround();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void CreateGround()
    {
        for (int i = -50; i < 50; i++)
        {
            for (int j = -50; j < 50; j++)
            {
                GameObject.Instantiate(groundTile, new Vector3(i * 10, 0f, j * 10), Quaternion.identity);
            }
        }
    }
}
